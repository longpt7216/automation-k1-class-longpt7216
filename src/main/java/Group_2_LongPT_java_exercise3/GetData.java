package Group_2_LongPT_java_exercise3;

import Group_2_LongPT_java_exercise3.object.PeopleInSchool;
import Group_2_LongPT_java_exercise3.object.Student;

import Group_2_LongPT_java_exercise3.object.Teacher;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;


import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.*;


public class GetData {

    public static String DataNo,type,name,gender,birthday,address,teachClass;
    public static int hourlyPay, teachingHoursPerMonth, studentID;
    public static float gpa;





    public static List<PeopleInSchool> getListPeopleData() {
        Map<String, String> map;
        List<PeopleInSchool> listPeople = new ArrayList<>();
        try {
            Reader reader = new FileReader("src/main/java/Group_2_LongPT_java_exercise3/resouce/PeopleData.csv");

            Iterator<Map<String, String>> iterator = new CsvMapper()
                    .readerFor(Map.class)
                    .with(CsvSchema.emptySchema().withHeader())
                    .readValues(reader);

            while (iterator.hasNext()) {
                map = iterator.next();
                type = map.get("type").trim();
                name = map.get("name").trim();
                gender = map.get("gender").trim();
                birthday = map.get("birthday").trim();
                address = map.get("address").trim();
                //Student
                if (type.equals("1")) {
                    studentID = Integer.parseInt(map.get("studentID").trim());
                    gpa = Float.parseFloat(map.get("gpa").trim());
                    listPeople.add(new Student(name, gender, birthday, address, studentID, gpa));
                    //teacher
                } else if (type.equals("2")) {
                    teachClass = map.get("teachclass").trim();
                    hourlyPay = Integer.parseInt(map.get("hourlyPay").trim());
                    teachingHoursPerMonth = Integer.parseInt(map.get("teachingHoursPerMonth").trim());
                    listPeople.add(new Teacher(name, gender, birthday, address, teachClass, hourlyPay, teachingHoursPerMonth));

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listPeople;
    }

}
