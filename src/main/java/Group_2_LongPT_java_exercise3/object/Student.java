package Group_2_LongPT_java_exercise3.object;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Student extends PeopleInSchool implements Comparable<Student>{

    private static final int type = 2;
    private int studentID;
    private float gpa;

    private static final List<Integer> listID = new ArrayList<>();
    private static final List<Float> listGPA = new ArrayList<>();
    private static final double SCHOLARSHIPSCORE = 8.0;
    private static final List<Student> listStudent = new ArrayList<>();



    public Student(String name, String gender, String birhday, String address, int studentID, float gpa) {
        super(name, gender, birhday, address);
        this.studentID = studentID;
        this.gpa = gpa;
    }


    public int getStudentID() {
        return studentID;
    }

    public float getGpa() {
        return gpa;
    }


    @Override
    public void getAllInfo() {
        System.out.println("===================================" +
                "\nThông tin của Student " + getName() + " là : ");
        System.out.println(
                " Tên : " + getName() +
                        "\n Giới tính : " + getGender() +
                        "\n Ngày sinh : " + getBirhday() +
                        "\n Địa chỉ : " + getAddress());
        System.out.println("" +
                " StudentID : " +  String.format("%08d", getStudentID()) +
                "\n gpa : : " + getGpa());
    }

    @Override
    public int getType() {
        return type;
    }

    public static void SetListID(List<PeopleInSchool> peopleList){
        for (PeopleInSchool people : peopleList) {
            if (people.getType() == 2) {
                listID.add(people.getInfo());
            }
        }
    }

    public static List<Integer> GetListID(){
        return listID;
    }

    @Override
    protected int getInfo() {
        return studentID;
    }

    public static void whoCanTakeScholarship(List<PeopleInSchool> peopleList){
        System.out.println("Người được học bổng là : " );
        for (PeopleInSchool people : peopleList) {
            Student student;
            if (people.getType() == 2 ) {
                student = (Student) people;
                if (student.getGpa() >= SCHOLARSHIPSCORE) {
                    System.out.println(people.getName());
                }
            }
        }
    }

    public static void find(List<PeopleInSchool> peopleList, Integer keyword) {
        boolean foundOrNot = false;
        System.out.println("Kết quả :");
        for (PeopleInSchool people : peopleList) {
            if ((people.getType() == 2 && String.valueOf(people.getInfo()).contains(keyword.toString()))) {
                people.getAllInfo();
                foundOrNot = true;
            }
        }
        if (!foundOrNot) System.out.println("NOT FOUND !!!");
    }

    public void setGpa(float gpa) {
        this.gpa = gpa;
    }

    public static void sortStudentsByScore(List<PeopleInSchool> peopleList){
        for (PeopleInSchool people : peopleList) if (people.getType() == 2) listStudent.add((Student) people);
        for (Student student : listStudent)student.setGpa(student.getGpa() * 10); //becouse compareTo() return Int
        Collections.sort(listStudent);
        for (Student student : listStudent)student.setGpa(student.getGpa() / 10);
        System.out.println("List Student sau khi sort");

        for (Student student : listStudent) student.getAllInfo();

    }

    @Override
    public int compareTo(Student o){
        return (int) (o.getGpa() - gpa);
    }
}
