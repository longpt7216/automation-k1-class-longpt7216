package Group_2_LongPT_java_exercise3.object;

public abstract class PeopleInSchool {

    private final String name;
    private final String gender;
    private final String birhday;
    private final String address;


    public PeopleInSchool(String name, String gender, String birthday, String address) {
        this.name = name;
        this.gender = gender;
        this.birhday = birthday;
        this.address = address;
    }



    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getBirhday() {
        return birhday;
    }

    public String getAddress() {
        return address;
    }


    public abstract void getAllInfo();

    public abstract int getType();

    protected abstract int getInfo();


}
