package Group_2_LongPT_java_exercise3.object;


import java.text.NumberFormat;
import java.util.*;

public class Teacher extends PeopleInSchool{

    private static final int type = 1;
    private String teachClass;
    private int hourlyPay;
    private int teachingHoursPerMonth;
    private long salary;
    private final int BONUS = 5000000;

    private static final List<Teacher> listTeacher = new ArrayList<>();


    public Teacher(String name, String gender, String birhday, String address, String teachClass, int hourlyPay, int teachingHoursPerMonth) {
        super(name, gender, birhday, address);
        this.teachClass = teachClass;
        this.hourlyPay = hourlyPay;
        this.teachingHoursPerMonth = teachingHoursPerMonth;
        salary = ((long) this.hourlyPay * this.teachingHoursPerMonth) + BONUS;
    }

    public String getTeachClass() {
        return teachClass;
    }

    public int getHourlyPay() {
        return hourlyPay;
    }

    public int getTeachingHoursPerMonth() {
        return teachingHoursPerMonth;
    }

    @Override
    public void getAllInfo() {
        double money = getSalary();
        NumberFormat formatter = NumberFormat.getNumberInstance();
        String salaryAfter = formatter.format(money);

        if (getName().contains("ThangLD")) salaryAfter = "$1500 anyway - Truth revealed by NhungDT :)";

        System.out.println("--------------------------------------" +
                "\nThông tin của Teacher " + getName() + " là : ");
        System.out.println(
                " Tên : " + getName() +
                        "\n Giới tính : " + getGender() +
                        "\n Ngày sinh : " + getBirhday() +
                        "\n Địa chỉ : " + getAddress());
        System.out.println(
                " Teach Class : " + getTeachClass() +
                "\n Hourly Pay : " + getHourlyPay() +
                "\n Teaching Hours Per Month : : " + getTeachingHoursPerMonth() +
                "\n Salary : " + salaryAfter);


    }

//    public void setSalary(){
//        this.salary = this.hourlyPay * this.teachingHoursPerMonth + BONUS;
//    }

    public long getSalary(){
        return salary;
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    protected int getInfo() {
        return 0;
    }


    public static void findHighestSalary(List<PeopleInSchool> peopleList){
        boolean foundOrNot = false;
        long findHighestSalary = 0;

        System.out.println("Kết quả :");
        for (PeopleInSchool people : peopleList) {
            Teacher teacher ;
            if (people.getType() == 1){
                teacher = (Teacher) people;
                if (teacher.getSalary() > findHighestSalary) {
                    findHighestSalary = teacher.getSalary();
                    foundOrNot = true;
                }

            }

        }
        for (PeopleInSchool people : peopleList) {
            Teacher teacher ;
            if (people.getType() == 1) {
                teacher = (Teacher) people;
                if (teacher.getSalary() == findHighestSalary) people.getAllInfo();
            }

        }
        if (!foundOrNot) System.out.println("NOT FOUND !!!");
    }




    public static void find(List<PeopleInSchool> peopleList, String keyword) {
        boolean foundOrNot = false;
        System.out.println("Kết quả :");
        for (PeopleInSchool people : peopleList) {
            if (people.getType() == 1 && people.getName().contains(keyword)) {
                people.getAllInfo();
                foundOrNot = true;
            }
        }
        if (!foundOrNot) System.out.println("NOT FOUND !!!");
    }

    public static void sortTeacherBySalary(List<PeopleInSchool> peopleList){
        //sort double bằng cơm vì override Collection trả về Int sẽ ko count đc tiền nhiều
        List<Long> listTeacherSalary = new ArrayList<>();
        List<Long> listSalarySorted = new ArrayList<>();

        for (PeopleInSchool people : peopleList) if (people.getType() == 1) {
            listTeacher.add((Teacher) people);
            listTeacherSalary.add(((Teacher) people).getSalary());
        }

        do{
            listSalarySorted.add(Collections.max(listTeacherSalary));
            listTeacherSalary.remove(Collections.max(listTeacherSalary));
            if (listTeacherSalary.isEmpty()) break;
        } while(true);

        for (Long salary : listSalarySorted) {
            for (Teacher teacher : listTeacher) if (teacher.getSalary() == salary) teacher.getAllInfo();
        }

    }


}
