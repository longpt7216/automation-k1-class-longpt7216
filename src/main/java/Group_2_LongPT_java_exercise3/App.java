package Group_2_LongPT_java_exercise3;


import Group_2_LongPT_java_exercise3.object.PeopleInSchool;
import Group_2_LongPT_java_exercise3.object.Student;
import Group_2_LongPT_java_exercise3.object.Teacher;

import java.util.*;

public class App {

    static boolean wantToQuit;
    public static String DataNo,type,name,gender,birthday,address,teachClass;
    public static int hourlyPay, teachingHoursPerMonth, studentID;
    public static float gpa;

    static final List<PeopleInSchool> peopleList = GetData.getListPeopleData();
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args){

/*

        -Student có các thông tin:
        Tên,
                Giới tính,
                Ngày sinh,
                Địa chỉ,
                Mã sinh viên (8 số - Không trùng với các student khác),
        Điểm trung bình (0~10).

        -Teacher có các thông tin:
        Tên,
                Giới tính,
                Ngày sinh,
                Địa chỉ,
                Lớp dạy,
                Lương một giờ dạy,
                Số giờ dạy trong tháng

        -File input.txt chứa các thông tin của Student và Teacher:
        +Thông tin của một Student hoặc Teacher được nằm trên mỗi dòng.
                +Thông số đầu tiên của mỗi dòng là chức vụ: 1 <=> Student, 2 <=> Teacher.

                YÊU CẦU:
        DONE 1. Thiết kế và viết code của chương trình trên làm sao để tuân thủ theo đúng mô hình OOP bao gồm: bao đóng (encapsulation), kế thừa (inheritance)
        DONE 2. Xác định và viết code constructor, getter, setter cho tất cả các class.
        DONE 3. Sử dụng và giải thích được ý nghĩa của 2 keyword: super, this trong phần thiết kế code ở trên.
        DONE 4. Đọc file input.txt và lưu thông tin vào danh sách Student, Teacher tương ứng.
        DONE 5. Hiển thị thông tin của Student, Teacher (Lấy từ danh sách đã đọc được ở (4), không lấy dữ liệu trực tiếp từ file đưa ra console)
        DONE 6. Chương trình cho phép thêm một Student hoặc một Teacher (Nhập từ bàn phím)
        DONE 7. Tìm ra Student được học bổng (điểm trung bình >= 8.0)
        DONE 8. Tìm Student theo mã, Teacher theo tên (tìm gần đúng)
        DONE 9. Tìm ra giáo viên có lương cao nhất (Lương = lương một giờ dạy * số giờ dạy trong tháng + 5000000)
        Done 10. Sắp xếp Student theo điểm, Teacher theo lương
*/


        boolean wantToQuitMainMenu = false;

        do{
            System.out.println("=================================\n=================================\n=================================" +
                    "\nChọn Chương Trình +" +
                    "\n\t 1 : Show List Teacher và Student hiện có" +
                    "\n\t 2 : Thêm một Teacher hoặc Student" +
                    "\n\t 3 : Tìm ra Student được học bổng (điểm trung bình >= 8.0)" +
                    "\n\t 4 : Tìm Student, Teacher (tìm gần đúng)" +
                    "\n\t 5 : Tìm ra giáo viên có lương cao nhất" +
                    "\n\t 6 : Sắp xếp Student theo điểm, Teacher theo lương" +
                    "\n\t 0 : Quit" );
            int choice = scanner.nextInt();
            switch (choice) {


                case 1 :
                    showInfo();
                    break;
                case 2 :
                    addPeople();
                    break;
                case 3 :
                    Student.whoCanTakeScholarship(peopleList);
                    break;
                case 4 :
                    find();
                    break;
                case 5 :
                    Teacher.findHighestSalary(peopleList);
                    break;
                case 6 :
                    sortPeople();
                    break;
                case 0 :
                    wantToQuitMainMenu = true;
                    break;
                default :
                    System.out.println("Hãy nhập ký tự từ 1~3");
            }
        }while (!wantToQuitMainMenu);

    }



    private static void sortPeople() {
        wantToQuit = false;

        do{
            System.out.println("=================================\n=================================\n=================================" +
                    "\nChọn đối tượng muốn thêm +" +
                    "\n\t 1 : Sắp xếp Teacher theo lương" +
                    "\n\t 2 : Sắp xếp Student theo điểm" +
                    "\n\t 3 : Back");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1 :
                    Teacher.sortTeacherBySalary(peopleList);
                    break;
                case 2 :
                    Student.sortStudentsByScore(peopleList);
                    break;
                case 3 :
                    wantToQuit = true;
                    break;
                default :
                    System.out.println("Hãy nhập ký tự từ 1~3");
                    break;
            }
        }while (!wantToQuit);

    }

    private static void find() {
        wantToQuit = false;
        String keywordTeacher;
        int keywordStudent;

        do{
            System.out.println("=================================\n=================================\n=================================" +
                    "\nChọn đối tượng muốn thêm +" +
                    "\n\t 1 : Tìm Teacher" +
                    "\n\t 2 : Tìm Student" +
                    "\n\t 3 : Back" );
            int choice = scanner.nextInt();
            switch (choice) {
                case 1 :
                    scanner = new Scanner(System.in);
                    System.out.println("Hãy nhập tên Teacher cần tìm : ");
                    keywordTeacher = scanner.nextLine();
                    Teacher.find(App.peopleList, keywordTeacher);
                    break;
                case 2 :
                    scanner = new Scanner(System.in);
                    System.out.println("Hãy nhập ID của Student cần tìm : ");
                    keywordStudent = scanner.nextInt();
                    Student.find(App.peopleList, keywordStudent);
                    break;
                case 3 :
                    wantToQuit = true;
                    break;
                default :
                    System.out.println("Hãy nhập ký tự từ 1~3");
                    break;
            }
        }while (!wantToQuit);

    }


    private static void addPeople() {
        wantToQuit = false;

        do{
            System.out.println("=================================\n=================================\n=================================" +
                    "\nChọn đối tượng muốn thêm +" +
                    "\n\t 1 : Add Teacher" +
                    "\n\t 2 : Add Student" +
                    "\n\t 3 : Back" );
            int choice = scanner.nextInt();
            switch (choice) {
                case 1 :
                    scanner = new Scanner(System.in);
                    System.out.println("Hãy nhập tên Teacher : ");
                    name = scanner.nextLine();
                    System.out.println("Hãy nhập tên giới tính là Male hoặc Female : ");
                    gender = scanner.nextLine();
                    System.out.println("Hãy nhập ngày sinh : ");
                    birthday = scanner.nextLine();
                    System.out.println("Hãy nhập địa chỉ : ");
                    address = scanner.nextLine();
                    System.out.println("Hãy nhập lớp dạy : ");
                    teachClass = scanner.nextLine();
                    System.out.println("Hãy nhập lương theo giờ : ");
                    hourlyPay = scanner.nextInt();
                    System.out.println("Hãy nhập giờ dạy trong tháng : ");
                    teachingHoursPerMonth = scanner.nextInt();
                    peopleList.add(new Teacher(name, gender, birthday, address, teachClass, hourlyPay, teachingHoursPerMonth));
                    break;
                case 2 :
                    scanner = new Scanner(System.in);
                    System.out.println("Hãy nhập tên Student : ");
                    String name = scanner.nextLine();
                    System.out.println("Hãy nhập tên giới tính là Male hoặc Female : ");
                    gender = scanner.nextLine();
                    System.out.println("Hãy nhập ngày sinh : ");
                    birthday = scanner.nextLine();
                    System.out.println("Hãy nhập địa chỉ : ");
                    address = scanner.nextLine();
                    System.out.println("Hãy nhập điểm trung bình của Student : ");
                    gpa = scanner.nextFloat();
                    //Student ID
                    Student.SetListID(peopleList);
                    studentID = Collections.max(Student.GetListID()) + 1;
                    peopleList.add( new Student(name, gender, birthday, address, studentID, gpa));
                    break;
                case 3 :
                    wantToQuit = true;
                    break;
                default :
                    System.out.println("Hãy nhập ký tự từ 1~3");
                    break;
            }
        }while (!wantToQuit);

    }



    private static void showInfo() {
            wantToQuit = false;

            do{
                System.out.println("====================================================================================================================================" +
                        "\n====================================================================================================================================" +
                        "\n===================================================================================================================================" +
                        "\nChọn Chương Trình +" +
                        "\n\t 1 : Show List Teacher" +
                        "\n\t 2 : Show List Student" +
                        "\n\t 3 : Show Tất cả" +
                        "\n\t 4 : Back" );
                int choice = scanner.nextInt();

                switch (choice) {
                    case 1 :
                        for (PeopleInSchool people : peopleList) {
                            if (people.getType() == 1) {
                                people.getAllInfo();
                            }
                        }
                        break;
                    case 2 :
                        for (PeopleInSchool people : peopleList) {
                            if (people.getType() == 2) {
                                people.getAllInfo();
                            }
                        }
                        break;
                    case 3 :
                        for (PeopleInSchool people : peopleList) people.getAllInfo();
                        break;
                    case 4 :
                        wantToQuit = true;
                        break;
                    default :
                        System.out.println("Hãy nhập ký tự từ 1~4");
                }
            }while (!wantToQuit);
        }


}
