package Group_2_LongPT_java_exercise1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Group_2_LongPT_java_exercise2_r1 {

    public static ArrayList<Integer> list = new ArrayList<>();
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
/*
        1.Nhập một Arraylist gồm n phần tử kiểu nguyên int.
        2.In giá trị của các phần tử a.
        3.Đếm số lượng phần tử lẻ trong arraylist a.
        4.Tính tổng số dương lẻ của a.
        5.Nhập vào phần tử k, tìm xem k có xuất hiện trong a không. Nếu có chỉ ra các vị trí của k trong arraylist.
        6.Sắp sếp a theo thứ tự tăng dần. (Có thể sử dụng Collection sort để xử lý).
        7.Đảo thứ tự các phần tử của a.  (Có thể sử dụng Collection sort để xử lý).
        8.Xóa các phần tử trong a có giá trị k được nhập vào từ bàn phím.
        9.Chèn 1 phần tử vào ví trí k bất kỳ (xóa bớt phần tử cuối cùng).
        10.Tìm giá trị lớn nhất và nhỏ nhất của a
        11.Tìm giá trị lớn nhì của a.
*/


        testAddElement();

        addElement();
        printList();
        printOddNumber();
        countOddNumber();
        sumOddNumber();
        searchNumberInList();
        sortList();
        revertList();
        deleteNumber();
        insertNumberAndDeleteLastElement();
        findLargestNumberAndSmalllestNumber();
        findSecondLargestNumber();
    }

    private static void testAddElement() {
        Object obj = new Object();

        ArrayList<Object> list = new ArrayList <>();
        list.add("ddd");
        list.add(2);
        list.add(11122.33);
        System.out.println(list);
    }


    public static void addElement() {
        System.out.println("Hãy nhập số lượng phần tử của List : ");
        int n = scanner.nextInt();
        System.out.println("Hãy nhập [ " + n + " ] phần tử cho List : ");
        for (int i = 0 ; i < n ; i++) list.add(scanner.nextInt());
    }

    private static void findSecondLargestNumber() {
        ArrayList<Integer> temp = list;
        temp.sort(Collections.reverseOrder());
        int number = Collections.max(temp);
        for (Integer integer : temp) {
            if (integer < number) {
                number = integer;
                break;
            }
        }
        System.out.println("Giá trị lớn nhì của List là : " + number) ;
        temp = null;
    }

    private static void findLargestNumberAndSmalllestNumber() {
        System.out.println("Giá trị lớn nhất của List : " + Collections.max(list) + " . Giá trị nhỏ nhất của List : " + Collections.min(list));
    }


    private static void insertNumberAndDeleteLastElement() {
        System.out.println("List " + toString(list) + " có : " + list.size() + " vị trí . Hãy nhập vị trí cần Insert Element từ 1 ~ " + list.size() );
        System.out.println("Hãy nhập số vị trí cần chèn : ");
        int location = scanner.nextInt() -1; // Nhìn theo view user thì giá trị bắt đầu từ [1]

        System.out.println("Hãy nhập số cần cần chèn : ");
        int number = scanner.nextInt();

        list.remove(list.size() - 1);
        list.add(location, number);

        System.out.println("Giá trị của tất cả các phần tử sau khi insert 1 phần tử: " + toString(list));
    }

    private static void deleteNumber() {
        System.out.println("Hãy nhập số cần xóa : ");
        int number = scanner.nextInt();
        boolean isRemoved = list.removeIf(n -> n == number && (list.contains(number)));
        String exist = "Xóa thành công !";
        String nonExist = "Số : " + number + " không tồn tại trong ArrayList";
        System.out.println(isRemoved ? exist : nonExist);
        System.out.println("Giá trị của tất cả các phần tử sau khi xóa 1 phần tử : " + toString(list));
    }

    private static void sortList() {
        Collections.sort(list);
        System.out.println("Giá trị của tất cả các phần tử sau khi sắp sếp theo thứ tự tăng dần: " + toString(list));
    }

    private static void revertList() {
        list.sort(Collections.reverseOrder());
        System.out.println("Giá trị của tất cả các phần tử sau khi sắp sếp theo thứ tự giảm dần :" + toString(list));
    }

    private static void searchNumberInList() {
        System.out.println("Nhập giá trị của phần từ cần tìm :");

        int number = scanner.nextInt();
        String exist = "Vị trí của số : " + number + " là : " + list.indexOf(number);
        String nonExist = "Số : " + number + " không tồn tại trong ArrayList";
        System.out.println((list.contains(number)) ? exist : nonExist);
    }

    private static void sumOddNumber() {
        int sum = 0;
        for (int i : list) if (i % 2 == 1) sum+= i;
        System.out.println("Tổng số dương lẻ : " + sum);
    }

    private static void countOddNumber() {
        int sum = 0;
        for (int i : list) if (i % 2 == 1) sum++;
        System.out.println("Số lượng phần tử lẻ trong arraylist : " + sum);
    }

    private static void printOddNumber() {
        System.out.println("Giá trị của các phần tử lẻ :");
        for (int i : list)  if (i % 2 == 1) System.out.println(i);
    }

    private static void printList() {
        System.out.println("Giá trị của tất cả các phần tử :" + toString(list));
    }

    public static String toString(ArrayList a) {
        if (a == null)
            return "null";

        int iMax = a.size() - 1;
        if (iMax == -1)
            return "";

        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; ; i++) {
            b.append(a.get(i));
            if (i == iMax)
                return b.append(']').toString();
            b.append(", ");
        }
    }
}
