
package Group_2_LongPT_java_exercise1;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


/*
LongPT7216
*/


public class Group_2_LongPT_java_exercise1 {
    public static Scanner scanner;

    public static void main(String[] args) {
        menu();
    }

    public static void menu() {
        scanner = null;
        boolean wantToQuit = false;
        int choice = 0;


        do{
            System.out.println("=============================");
            System.out.println("Hãy nhập số từ 1 đến 7" +
                    "\n\t1.Nhập năm sinh của một người. Tính tuổi người đó." +
                    "\n\t2.Nhập 2 số a và b. Tính tổng, hiệu, tính và thương của hai số đó." +
                    "\n\t3.Nhập tên sản phẩm, số lượng và đơn giá. Tính tiền và thuế giá trị gia tăng phải trả." +
                    "\n\t4.Nhập điểm thi và hệ số 3 môn Toán, Lý, Hóa của một học sinh. Tính điểm trung bình của học sinh đó." +
                    "\n\t5.Nhập bán kính của đường tròn. Tính chu vi và diện tích của hình tròn đó." +
                    "\n\t6.Nhập vào số xe của bạn (gồm tối đa 5 chữ số, mặc định đầu vào đúng). Cho biết số xe của bạn được mấy điểm." +
                    "\n\t7.Thoát chương trình" +
                    "\n=============================");

                try{
                    scanner = new Scanner(System.in);
                    choice = scanner.nextInt();
                }catch (Exception e){
                    System.out.println("ERROR : Hãy nhập số từ 1 đến 7");
                    choice = 0;
                }
//

            switch (choice){
                case 1:
                    calculateAge();
                    break;
                case 2:
                    calculate2Number();
                    break;
                case 3:
                    calculatePriceAndTax();
                    break;
                case 4:
                    calculateTestScore();
                    break;
                case 5:
                    calculatePerimeterAndArea();
                    break;
                case 6:
                    calculatePlateNumber();
                    break;
                case 7:
                    wantToQuit = true;
                    break;
                default:
                    System.out.println("Please Enter 1 to 7");
                    break;
            }
        }while(!wantToQuit);
    }

    private static void calculatePlateNumber() {
        int n1, n2, n3, n4, n5, soDiem;
        System.out.println("Hãy nhập biển số xe");
        int n = scanner.nextInt();
        n5 = n % 10; n = n / 10;
        n4 = n % 10; n = n / 10;
        n3 = n % 10; n = n / 10;
        n2 = n % 10; n = n / 10;
        n1 = n;
        soDiem = (n1 + n2 + n3 + n4 + n5) % 10;
        System.out.println("Số điểm của biển số xe là : " + soDiem);
    }

    private static void calculatePerimeterAndArea() {
        final double PI = 3.14159;
        int r;
        double perimeter, area;
        System.out.println("Nhập bán kính: ");
        r = scanner.nextInt();
        perimeter = 2*PI*r;
        area = PI*r*r;
        System.out.println("Diện tích là : " + area + ". Và chu vi là : " + perimeter);
    }

    private static void calculateTestScore() {
        // điểm
        int toan, li, hoa, hs;

        //hệ số từng môn của 3 môn toán lý hóa
        int t, l, h;

        //input
        float tb;
        System.out.println("Nhập điểm môn Toán: ");
        toan = scanner.nextInt();
        System.out.println("Nhập HỆ SỐ điểm môn Toán: ");
        t = scanner.nextInt();
        System.out.println("Nhập điểm môn Lý: ");
        li = scanner.nextInt();
        System.out.println("Nhập HỆ SỐ điểm môn Lý: ");
        l = scanner.nextInt();
        System.out.println("Nhập điểm môn Hóa: ");
        hoa = scanner.nextInt();
        System.out.println("Nhập HỆ SỐ điểm môn Hóa: ");
        h = scanner.nextInt();

        // count score
        hs = t + l + h;
        tb=(float)((toan*t)+(li*l)+(hoa*h))/hs;

        System.out.println("Điểm trung bình là : " + tb );
    }

    private static void calculatePriceAndTax() {
//
//        3.Nhập tên sản phẩm, số lượng và đơn giá. Tính tiền và thuế giá trị gia tăng phải trả, biết:
//        a. tiền = số lượng * đơn giá f
//        b. thuế giá trị gia tăng = 10%
//


        System.out.println("Hãy nhập tên sản phẩm ");
        String productName = scanner.next();
        System.out.println("Hãy nhập số lượng sản phẩm ");
        int productQuanlity = scanner.nextInt();
        System.out.println("Hãy nhập đơn giá ");
        int price = scanner.nextInt();

        int sum = productQuanlity * price;
        double tax = 0.1 * sum;
        sum += tax;

        System.out.println("Số tiền cho " + productQuanlity + " sản phẩm [ " + productName + " ] là : " + sum);

    }

    private static void calculate2Number() {
        System.out.println("Hãy nhập số a ");
        int numA = scanner.nextInt();
        System.out.println("Hãy nhập số b ");
        int numB = scanner.nextInt();
        System.out.println(numA + " + " + numB + " = " + (numA + numB));
        System.out.println(numA + " - " + numB + " = " + (numA - numB));
        System.out.println(numA + " * " + numB + " = " + (numA * numB));
        System.out.println(numA + " / " + numB + " = " + (numA / (double)numB));

    }

    private static void calculateAge() {
        System.out.println("Hãy nhập năm sinh của bạn ");
        int yearOfBirth = scanner.nextInt();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        Date date = new Date();

        System.out.println("Năm sinh của bạn là : " + yearOfBirth + " . Và tuổi của bạn là :  " + (Integer.parseInt(formatter.format(date)) - yearOfBirth ));
    }


}

