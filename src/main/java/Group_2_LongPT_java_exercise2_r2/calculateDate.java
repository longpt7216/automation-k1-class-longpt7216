package Group_2_LongPT_java_exercise2_r2;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.Scanner;

public class calculateDate {
    public static Scanner scannerDate;
    public static final DateTimeFormatter FORMATERS = DateTimeFormatter.ofPattern("dd/MM/uuuu");
    public static String dateNo1, dateNo2;

    public static void taskStart() {
        System.out.println("\n========= START chương trình DATE =========");
        inputDate();
    }

    public static void inputDate() {
        dateNo1 = "";
        dateNo2 = "";
        scannerDate = new Scanner(System.in);
        System.out.println("Hãy nhập ngày tháng năm theo dạng DD/MM/YYYY cho Date thứ nhất :");
        dateNo1 = scannerDate.next();
        System.out.println("Hãy nhập ngày tháng năm theo dạng DD/MM/YYYY cho Date thứ hai :");
        dateNo2 = scannerDate.next();
        scannerDate= null;

//        FOR TEST
//        dateNo1 = "21/06/1989";
//        dateNo2 = "12/02/2022";
//
//        dateNo1 = "29/02/2022"; // Ngày không tồn tại
//        dateNo2 = "31/04/2022"; // Ngày không tồn tại

//        dateNo1 = "2a/06/1989";
//        dateNo2 = "12/0a/2022";
//
//        dateNo1 = "21/06/198a";
//        dateNo2 = "12/02/20221";
//
//        dateNo1 = "213/06/1989";
//        dateNo2 = "12/022/2022";

        // Check validate for date
        if (!isValid(dateNo1)) inputDate();
        if (!isValid(dateNo2)) inputDate();

        calculateBetween2Date(dateNo1, dateNo2);
    }

    private static void calculateBetween2Date(String dateNo1, String dateNo2){
        LocalDate no1 = LocalDate.parse(dateNo1,FORMATERS);
        LocalDate no2 = LocalDate.parse(dateNo2,FORMATERS);

        Period diff = Period.between(
                no1, no2);
        System.out.println("Số ngày chênh lệch giữa 2 ngày đã nhập vào là: "
                        + diff.getYears()
                        + " năm, "
                        + diff.getMonths()
                        + " tháng, "
                        + diff.getDays()
                        + " ngày"
        );
    }


    public static boolean isValid(final String date) {
        try {
            // ResolverStyle.STRICT for 30, 31 days checking, and also leap year.
            LocalDate.parse(date,FORMATERS.withResolverStyle(ResolverStyle.STRICT));
            System.out.println("Date is VALID !!!");
            return true;

        } catch (DateTimeParseException e) {
            System.out.println("ERROR E01 !!! Bạn đã nhập sai định dạng ngày tháng . Xin hãy nhập lại !!!");
            return false;
        }
    }
}
