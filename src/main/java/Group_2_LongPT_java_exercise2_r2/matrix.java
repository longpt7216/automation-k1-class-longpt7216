package Group_2_LongPT_java_exercise2_r2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

public class matrix {
    private static int row, collum;
    private static final ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();
    private static final ArrayList<Integer> largestNumberList = new ArrayList<>();

    /*
        2).Viết chương trình thực hiện công việc sau:
        - Nhập ma trận A (m dòng, n cột) gồm các phần tử kiểu int. Sau đó, in ma trận A.
                - Tính tích các số là bội số của 3 nằm trên dòng đầu tiên của ma trận.
        - Tạo ra mảng một chiều X với X[i] là các giá trị lớn nhất trên từng dòng i của ma trận A. In X ra.
*/

    public static void taskStart() {
        System.out.println("\n======= START chương trình về Matrix ========");
        inputAndPrintMatrix();
        calculateMatrix();
        createLargestNumberList();
    }

    private static void createLargestNumberList() {
        for (ArrayList<Integer> integers : matrix) largestNumberList.add(Collections.max(integers));
        System.out.println("Các giá trị lớn nhất trên từng dòng của ma trận là : ");
        for (int number : largestNumberList) System.out.println(number + "\t");
    }


    private static void inputAndPrintMatrix() {
        Scanner scanner = new Scanner(System.in);

        try {                                                                      //try catch for Validate
            System.out.println("Hãy nhập số hàng :");
            row = scanner.nextInt();
            System.out.println("Hãy nhập số cột :");
            collum = scanner.nextInt();
            if(row == 0 || collum == 0){
                System.out.println("ERROR E02 : Hãy nhập số lớn hơn 0!!!");
                inputAndPrintMatrix();
            }
        } catch (InputMismatchException e ){
            System.out.println("ERROR E03 : Hãy nhập ký tự là số !!!");
            inputAndPrintMatrix();
        }


        for(int i = 0; i < row; i++) {
            System.out.println("Hãy nhập số cho row :" + i);
            matrix.add(new ArrayList());
            for (int j = 0; j < collum; j++) {
                try {                                                                       //try catch for Validate
                    matrix.get(i).add(scanner.nextInt());
                } catch (InputMismatchException e ){
                    System.out.println("ERROR E04 : Hãy nhập ký tự là số !!!");
                    inputAndPrintMatrix();
                }
            }
        }

        System.out.println("Matrix của bạn là :");
        for (ArrayList<Integer> integers : matrix) {
            for(int number : integers) System.out.print(number + "\t");
            System.out.println("");
        }
        scanner.close();
    }

    private static void calculateMatrix() {
        int sum = 1;
        System.out.println("Các số là bội số của 3 nằm trên dòng đầu tiên của ma trận : ");

        for (int number : matrix.get(0)){
            if (number % 3 == 0) {                                  // nếu trong list có 1 số là 0 thì sum sẽ là 0
                System.out.print(number + "\t\t");
                sum *= number;
            }
        }
        System.out.println("\n" + "Tích các số là bội số của 3 nằm trên dòng đầu tiên của ma trận : " + sum);
    }
}
